const {makeThumbnails} = require('./../utils/thumbnail');
const {getPagesCountOfFile} = require("../utils/pdfParser");
const {getFileByKey, makeS3BatchInsertValuesAndExecute, deleteFile} = require("./storage");
const {insertProcessableFileData, setFileStatusFinished, setFileStatusInProgress, makeDBInsertValuesOfS3ObjectsAndExecute} = require('./database');

const newFileEventHandler = async fileKey => {
    try {
        const {fileHash, fileBuffer, fileName, ContentLength, sourceFileName, fileExtension} = await getFileByKey(fileKey);
        const {countOfPages} = await getPagesCountOfFile(fileBuffer, fileExtension);
        const fileSizeInKB = ContentLength / 1000;
        await insertProcessableFileData({fileHash, sourceFileName, fileName, fileExtension, fileSizeInKB, countOfPages});
        // await makeAndInsertThumbnailsForFile(fileBuffer, countOfPages, fileHash);
        return {fileBuffer, fileHash, countOfPages, fileKey}
    } catch (e) {
        console.log(e)
    }
};

const makeAndInsertThumbnailsForFile = async (fileBuffer, fileHash, fileKey, countOfPages) => {
    try {
        await setFileStatusInProgress(fileHash);
        const thumbnailsGMData = makeThumbnails(fileBuffer, countOfPages);
        const S3uploadValues = await makeS3BatchInsertValuesAndExecute(thumbnailsGMData, fileHash);
        await makeDBInsertValuesOfS3ObjectsAndExecute(S3uploadValues);
        await setFileStatusFinished(fileHash);
        await deleteFile(fileKey)
    } catch (e) {
        console.log(e)
    }
};

module.exports = {
    newFileEventHandler,
    makeAndInsertThumbnailsForFile
};
