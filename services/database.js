const {makeInsertNewFileQuery, makeBatchThumbnailsInsert, makeFileStatusInProgress, makeFileStatusFinished} = require("../utils/query");
const {thumbnailNameUnPatcher} = require("../utils/enocde");
const {makeUniqueKey} = require('./../utils/uuid');
const {dataBaseModule} = require('./../modules');

const insertProcessableFileData = ({fileHash, sourceFileName, fileName, fileExtension, fileSizeInKB, countOfPages}) => {
    const queryParams = makeInsertNewFileQuery({
        fileHash,
        sourceFileName,
        fileName,
        fileExtension,
        fileSizeInKB,
        countOfPages
    });
    dataBaseModule.addItem(queryParams)
};

const batchThumbnailResultsUpload = insertItems => {
    const queryParams = makeBatchThumbnailsInsert(insertItems);
    return dataBaseModule.batchWrite(queryParams);
};

const setFileStatusInProgress = fileHash => {
    const queryParams = makeFileStatusInProgress(fileHash);
    return dataBaseModule.updateItem(queryParams)
};
const setFileStatusFinished = fileHash => {
    const queryParams = makeFileStatusFinished(fileHash);
    return dataBaseModule.updateItem(queryParams)
};
const makeDBInsertValuesOfS3Objects = s3InsertResult => {
    return s3InsertResult.map(i =>
        Object.assign({}, {
            url: i.Location,
            uuid: makeUniqueKey(),
            ...thumbnailNameUnPatcher(i.key)
        })
    )
};

const makeDBInsertValuesOfS3ObjectsAndExecute = S3UploadValues => {
    const insertValues = makeDBInsertValuesOfS3Objects(S3UploadValues);
    return batchThumbnailResultsUpload(insertValues);
};

module.exports = {
    makeDBInsertValuesOfS3ObjectsAndExecute,
    setFileStatusFinished,
    setFileStatusInProgress,
    batchThumbnailResultsUpload,
    insertProcessableFileData
};
