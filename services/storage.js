const {makeHashOfFile, parsePatchedFileName, thumbnailNamePatcher} = require("../utils/enocde");
const {fileStorageModule, thumbnailStorageModule} = require('./../modules');

const getFileByKey = async key => {
    const {ContentLength, Body : fileBuffer} = await fileStorageModule.getFileByFileKey(key);
    const {ext : fileExtension, fileName, sourceFileName, nameForHash} = parsePatchedFileName(key);
    const fileHash = makeHashOfFile(fileBuffer, sourceFileName);
    return {fileHash, fileBuffer, sourceFileName, fileName, ContentLength, fileExtension}
};
const uploadThumbnail = (fileBuffer, fileName) => {
    return thumbnailStorageModule.uploadFile(fileBuffer, fileName, true)
};

const batchThumbnailsUpload = thumbnails => {
    const promises = thumbnails.map(({thumbnailBuffer, name}) => uploadThumbnail(thumbnailBuffer, name));
    return Promise.all(promises)
};


const makeS3InsertObject = (thumbnailGMData, thumbnailHash, page) => {
    return new Promise((resolve, reject) => {
        thumbnailGMData.stream((err, stdout, stderr) => {
            if (err) {
                return reject(err)
            }
            const chunks = [];
            stdout.on('data', (chunk) => {
                chunks.push(chunk)
            });

            stdout.once('end', () => {
                resolve({thumbnailBuffer: Buffer.concat(chunks), name: thumbnailNamePatcher(thumbnailHash, page)})
            });
            stderr.once('data', (data) => {
                reject(String(data))
            })
        })
    })
};
const makeS3BatchInsertValues = (thumbnailsGMData, thumbnailHash) => {
    const S3InsertPromises = thumbnailsGMData.map(({fileBuffer, page}) => makeS3InsertObject(fileBuffer, thumbnailHash, page));
    return Promise.all(S3InsertPromises);
};


const makeS3BatchInsertValuesAndExecute = async (thumbnailsGMData, fileHash) => {
    const insertValues = await makeS3BatchInsertValues(thumbnailsGMData, fileHash);
    return batchThumbnailsUpload(insertValues)
};

const deleteFile = fileKey => fileStorageModule.deleteFile(fileKey);
module.exports = {
    deleteFile,
    getFileByKey,
    batchThumbnailsUpload,
    makeS3BatchInsertValuesAndExecute
};
