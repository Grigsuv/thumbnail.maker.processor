const name = 'thumbnail-maker-thumbnails';
module.exports = {
    name,
    schema: {
        TableName: name,
        AttributeDefinitions: [
            {
                AttributeName: "uuid",
                AttributeType: "S"
            },
        ],
        KeySchema: [
            {
                AttributeName: "uuid",
                KeyType: "HASH"
            }
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 5,
            WriteCapacityUnits: 5
        },
    },
};
