const name = 'thumbnail-maker-files';
module.exports = {
    name,
    schema: {
        TableName: name,
        AttributeDefinitions: [
            {
                AttributeName: "fileHash",
                AttributeType: "S"
            }
        ],
        KeySchema: [
            {
                AttributeName: "fileHash",
                KeyType: "HASH"
            }
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 5,
            WriteCapacityUnits: 5
        },
    }
};
