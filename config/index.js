require('dotenv').config();
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

module.exports = {
    fileStorage: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        storageRegion: process.env.AWS_REGION,
        storageName: process.env.FILE_STOREGE_NAME || 'thumbnail-maker-files'
    },
    thumbnailStorage: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        storageRegion: process.env.AWS_REGION,
        storageName: process.env.THUMBNAIL_STOREGE_NAME || 'thumbnail-maker-thumbnails'
    },
    dataBase: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        dataBaseRegion: process.env.AWS_REGION,
    }
};
