const {newFileEventHandler, makeAndInsertThumbnailsForFile} = require('./services');

async function test(key) {
    const {fileBuffer, fileHash, countOfPages, fileKey} = await newFileEventHandler(key);
    await makeAndInsertThumbnailsForFile(fileBuffer, fileHash, fileKey, countOfPages)
}

const s3FileKey = process.argv[ process.argv.length - 1 ];
test(s3FileKey);
