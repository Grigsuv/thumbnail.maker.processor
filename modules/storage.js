const {S3} = require('aws-sdk');
const apiVersion = '2006-03-01';

class S3Storage {
    #S3Instance;
    #InstanceName;
    #accessLevel;
    #init = async () => {
        const {Buckets} = await this.#S3Instance.listBuckets().promise();
        if (!Buckets.find(bucket => bucket.Name === this.#InstanceName))
            this.#createBucket()
    };

    #createBucket = async () => {
        const bucketOptions = {
            ACL: this.#accessLevel,
            Bucket: this.#InstanceName
        };
        const {Location} = await this.#S3Instance.createBucket(bucketOptions).promise();
        console.log(`Created new bucket with name ${this.#InstanceName}, location ${Location}, accessLevel is ${this.#accessLevel}`)
    };

    constructor({accessKeyId, secretAccessKey, storageRegion, storageName, publicReadAccess = false}) {
        if (!accessKeyId || !secretAccessKey || !storageRegion || !storageName)
            throw new Error('Missing constructor params ');

        this.#S3Instance = new S3({accessKeyId, secretAccessKey, region: storageRegion, apiVersion});
        this.#InstanceName = storageName;
        this.#accessLevel = publicReadAccess ? "public-read" : "private";
        this.#init()
            .catch(e => {
                console.log(e);
                process.exit(-1)
            })
    }


    getFileByFileKey(fileKey) {
        const fileParams = {
            Bucket: this.#InstanceName,
            Key: fileKey
        };
        return this.#S3Instance.getObject(fileParams).promise();
    }

    getAllFiles() {
        const bucketParams = {
            Bucket: this.#InstanceName
        };
        return this.#S3Instance.listObjects(bucketParams).promise()
    }

    uploadFile(bufferOfFile, fileName, isPublic = false) {
        const fileParams = {
            Bucket: this.#InstanceName,
            Key: fileName,
            Body: bufferOfFile,
            ACL: isPublic ? 'public-read' : 'private'
        };
        return this.#S3Instance.upload(fileParams).promise()
    }

    deleteFile(fileKey) {
        const fileParams = {
            Bucket: this.#InstanceName,
            Key: fileKey
        };
        return this.#S3Instance.deleteObject(fileParams).promise()
    }
}


module.exports = S3Storage;
