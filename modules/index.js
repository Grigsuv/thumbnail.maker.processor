const config = require('./../config');
const Storage = require('./storage');
const DB = require('./db');

module.exports = {
  fileStorageModule : new Storage(config.fileStorage),
  thumbnailStorageModule: new Storage(config.thumbnailStorage),
  dataBaseModule: new DB(config.dataBase)
};
