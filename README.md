# Thumbnail.Maker.Processor 

### Description

The project is designed with AWS S3 event-driver architecture. It's uses `DynamoDB` for DB store management

### Create .env file with following keys

```
AWS_REGION
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
```

## Deployement

``` bash
npm i -g serverless
cd thumbnail.maker.processor && serverless deploy
```


## Running locally

```
npm run test ${YOUR_SC3FileKey}
```

