const {fileStorage} = require('./config');
const {newFileEventHandler, makeAndInsertThumbnailsForFile} = require('./services');
exports.handler = async (event, context) => {
    const bucket = event.Records[0].s3.bucket.name;
    const key = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, ' '));
    try {

        if (bucket !== fileStorage.storageName) return;
        const {fileBuffer, fileHash, countOfPages, fileKey} = await newFileEventHandler(key);
        await makeAndInsertThumbnailsForFile(fileBuffer, fileHash, fileKey, countOfPages)
    } catch (err) {
        console.log(err);
        const message = `Error getting object ${key} from bucket ${bucket}. Make sure they exist and your bucket is in the same region as this function.`;
        console.log(message);
        throw new Error(message);
    }
};
