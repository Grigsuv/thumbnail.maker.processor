const gm = require("gm").subClass({imageMagick: true});


const makeThumbnails = (fileBuffer, pagesCount) => {
    let result = [];
    for (let i = 0; i < pagesCount; ++i) {
        result = [...result,
            {
                fileBuffer: gm(fileBuffer, `file.pdf[${i}]`)
                    .flatten()
                    .setFormat("jpg")
                    .resize(200)
                    .quality(100),
                page: i + 1
            }
        ]
    }
    return result
};


module.exports = {
    makeThumbnails
};
