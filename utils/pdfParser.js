const pdfParser = require('pdf-parse');

const getPagesCountOfPDFInBuffer = async buffer => {
    if (!Buffer.isBuffer(buffer)) buffer = new Buffer.from(buffer);

    const {numpages, numrender} = await pdfParser(buffer);
    return Math.max(numpages, numrender)
};

const fileExtensionMap = {
    pdf: getPagesCountOfPDFInBuffer,
    default: () => 1
};
const getPagesCountOfFile = async (bufferOfFile, fileExtension) => {
    const filePageCountFinder = fileExtensionMap[fileExtension] || fileExtensionMap.default;

    return {countOfPages: await filePageCountFinder(bufferOfFile)}
};
module.exports = {
    getPagesCountOfFile,
};
