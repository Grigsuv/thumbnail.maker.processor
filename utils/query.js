const {fileModel, thumbnailsModel} = require('./../models');
const dynamoDBTypeMap = {
    string: 'S',
    number: 'N',
    boolean: 'BOOL',
    buffer: 'B',
    default: 'S'
};

const getDynamoDBFiledType = value => {
    if (Buffer.isBuffer(value)) return dynamoDBTypeMap.buffer;

    return dynamoDBTypeMap[typeof value] || dynamoDBTypeMap.default;
};
const fileStatuses = {
    new: 'new',
    inProgress: 'in progress',
    finished: 'finished'
};
const defaultValuesForNewFile = {
    status: fileStatuses.new
};
const emptyStringCheck = str => str === '';

const makeKeyValueMapWithDBTypes = items => {
    return Object.entries(items).reduce((acc, cur) => {
        const [key, value] = cur;
        if (emptyStringCheck(value)) return acc;
        const fieldType = getDynamoDBFiledType(value);
        acc[key] = {[fieldType]: `${value}`};
        return acc
    }, {})
};

const makeInsertQueryObject = (items, tableName) => {
    return {TableName: tableName, Item: makeKeyValueMapWithDBTypes(items)}
};


const makeInsertNewFileQuery = dataToInsert => {
    return makeInsertQueryObject({...dataToInsert, ...defaultValuesForNewFile}, fileModel.name)
};

const makeBatchInsert = (items, tableName) => {
    const result = items.reduce((acc, cur) => {
        const Item = makeKeyValueMapWithDBTypes(cur);
        acc = [...acc, {
            PutRequest: {
                Item
            }
        }
        ];
        return acc
    }, []);
    return {
        [tableName]: result
    }
};
const makeBatchThumbnailsInsert = thumbnails => makeBatchInsert(thumbnails, thumbnailsModel.name);


const makeUpdateQuery = (updateValues, updateKeys, tableName) => {
    return Object.entries(updateValues).reduce((acc, cur) => {
        const [key, value] = cur;
        if (emptyStringCheck(value)) return acc;

        const updateExpressionVariableName = `#${key}`;
        const updateExpressionValue = `:${key.toLowerCase()}`;
        const fieldType = getDynamoDBFiledType(value);

        return {
            ...acc,
            ExpressionAttributeNames: {...acc.ExpressionAttributeNames, [updateExpressionVariableName]: key},
            ExpressionAttributeValues: {
                ...acc.ExpressionAttributeValues,
                [updateExpressionValue]: {[fieldType]: value}
            },
            UpdateExpression:  `${acc.UpdateExpression} ${updateExpressionVariableName} = ${updateExpressionValue}`
        }
    }, {
        TableName: tableName,
        Key: makeKeyValueMapWithDBTypes(updateKeys),
        UpdateExpression: 'SET ',
        ExpressionAttributeNames: {},
        ExpressionAttributeValues: {}
    })
};

const updateFileTable = (updateValues, fileHash) => makeUpdateQuery(updateValues, {fileHash}, fileModel.name);
const makeFileStatusInProgress = fileHash => updateFileTable({status: fileStatuses.inProgress}, fileHash);
const makeFileStatusFinished = fileHash => updateFileTable({status: fileStatuses.finished}, fileHash);

module.exports = {
    makeFileStatusInProgress,
    makeFileStatusFinished,
    makeInsertNewFileQuery,
    makeBatchThumbnailsInsert
};
