const crypto = require('crypto');

const makeHashOfFile = (fileBuffer, name) => {
    if (!Buffer.isBuffer(fileBuffer)) fileBuffer = new Buffer.from(fileBuffer);
    let sum = crypto.createHash('sha256');
    sum.update(fileBuffer);
    if (name) sum.update(Buffer.from(name));
    return sum.digest('hex');
};
const unpatchFileName = fileName => {
    const patch = fileName.slice(fileName.lastIndexOf(':'), fileName.lastIndexOf('.'));
    return patch ? fileName.replace(patch, '') : fileName
};

const parsePatchedFileName = fileName => {
    const sourceFileName = unpatchFileName(fileName);
    const splitedName = sourceFileName.split('.');
    const ext = splitedName.pop();

    return {ext, fileName: splitedName.join('.'), sourceFileName, nameForHash: fileName}
};

const thumbnailNamePatcher = (fileHash, pageNumber) => `${fileHash}:${pageNumber}`;
const thumbnailNameUnPatcher = patchedName => {
    const lastIndexOfPatch = patchedName.lastIndexOf(':');
    if (lastIndexOfPatch < 0) throw new Error('Filename not patched');
    return {
        fileHash: patchedName.slice(0, lastIndexOfPatch ),
        pageNumber: patchedName.slice(lastIndexOfPatch + 1)
    }
};
module.exports = {
    thumbnailNameUnPatcher,
    thumbnailNamePatcher,
    makeHashOfFile,
    parsePatchedFileName
};
