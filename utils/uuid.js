const {v4: uuidV4} = require('uuid');

const makeUniqueKey = () => uuidV4();


module.exports = {
    makeUniqueKey
};
